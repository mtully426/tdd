import org.junit.Test;
import static org.junit.Assert.*;

public class TestUrl {

    @Test
    public void getProtocolFromUrl(){
        Url launchcode = new Url("https://launchcode.org/");
        assertEquals("https", launchcode.getProtocol());

    }

    @Test
    public void getDomainFromUrl(){
        Url launchcode = new Url("hTTps://www.lAunChcode.org/to/a/path");
        assertEquals("www.launchcode.org", launchcode.getDomain());


    }

    @Test
    public void getPathFromUrl(){
        Url launchcode = new Url("hTTps://www.lAunChcode.org/Classes/first/new");
        assertEquals("/classes/first/new", launchcode.getPath());
        }

    @Test
    public void testToString(){
        Url launchcode = new Url("hTTps://www.lAunChcode.org/Classes/first/new");
        assertEquals("https://www.launchcode.org/classes/first/new", launchcode.toString());
    }

    @Test
    public void noPath(){
        Url google = new Url("http://www.google.com");
        assertEquals("http", google.getProtocol());
        assertEquals("www.google.com", google.getDomain());
        assertEquals("/", google.getPath());
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalCharacter(){
        Url google = new Url("http://www.as!fsdd.net");
        fail("Should not get here");
    }

    @Test(expected = IllegalArgumentException.class)
    public void noDomain(){
        Url google = new Url("http://");
        fail("Should not get here");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidProtocol(){
        Url google = new Url("htp://www.google.com");
        fail("Shouldn't get here");
    }

    @Test
    public void findWithPort(){
        Url google = new Url("https://www.google.com:5454/get/in/the/choppa");
        assertEquals("5454", google.getPort());
        assertEquals("www.google.com", google.getDomain());
        assertEquals("/get/in/the/choppa", google.getPath());

    }

    @Test
    public void findWithQuery(){
        Url google = new Url("https://www.google.com/get/in/the/?quote=get%in%the%choppa");
        assertEquals("quote=get%in%the%choppa", google.getQuery());
        assertEquals("www.google.com", google.getDomain());
        assertEquals("/get/in/the/", google.getPath());
        assertEquals("https://www.google.com/get/in/the/?quote=get%in%the%choppa", google.toString());

    }
}
