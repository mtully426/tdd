import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Url {

    final private String[] ALLOWED_PATHS = {"http", "ftp", "https", "file"};
    final private String protocol;
    final private String domain;
    final private String path;
    final private String port;
    final private String query;


    public Url(String url){
        protocol = determineProtocol(url);
        domain = determineDomain(url);
        path = determinePath(url);
        port = determinePort(url);
        query = determineQuery(url);

    }



    public String getDomain() {
        return this.domain;
    }

    public String getProtocol(){
        return this.protocol;
    }

    public String getPath(){
        return this.path;
    }

    public String getPort(){
        return this.port;
    }

    public String getQuery(){
        return this.query;
    }
    private String determineProtocol(String url){
        String protocol = url.substring(0, url.indexOf(':')).toLowerCase();

        if(!Arrays.asList(ALLOWED_PATHS).contains(protocol)){
            throw new IllegalArgumentException("Invalid protocol");
        }
        return protocol;
    }

    private String determineDomain(String url){
        Pattern pattern = Pattern.compile("//(.*?)(/|$|:)");
        Matcher matcher = pattern.matcher(url);
        String domain = "";
        if(matcher.find()){
            domain = matcher.group(1);
        }
        pattern = Pattern.compile("[^a-zA-Z0-9._-]");
        matcher = pattern.matcher(domain);
        if(matcher.find()){
            throw new IllegalArgumentException("Invalid character");
        }
        if(domain.length() < 1){
            throw new IllegalArgumentException("Invalid domain");
        }

        return domain.toLowerCase();
        //return url.substring(url.indexOf("//"), url.indexOf('/')).toLowerCase();
    }

    private String determinePath(String url){
        Pattern pattern = Pattern.compile("(?:.*?/){3}(.*)(\\?|$)");
        Matcher matcher = pattern.matcher(url);
        String domain = "";
        if(matcher.find()){
            domain = matcher.group(1);
        }
        return "/"+domain.toLowerCase();

   }

   private String determinePort(String url){
        long numColon = url.chars().filter(ch -> ch == ':').count();
        if(numColon <= 1){
            return "";
        } else {

            String newPort = url.split(":")[2];
            Pattern pattern = Pattern.compile("^(.*?)(/|$)");
            Matcher matcher = pattern.matcher(newPort);
            if (matcher.find()) {
                newPort = matcher.group(1);
            }
            return newPort;
        }
   }

   private String determineQuery(String url){
        if(url.contains("?")) {
            String theQuery = url.split("\\?")[1];
            return theQuery;
        } else{
            return "";
        }

   }

   @Override
    public String toString(){

        if(port.length() < 1){
            return protocol+"://"+domain+path+query;
        } else{
            return protocol+"://"+domain+":"+port+path+query;
        }

   }


}
